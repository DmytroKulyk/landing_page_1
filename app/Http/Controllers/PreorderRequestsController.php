<?php

namespace App\Http\Controllers;

//use App\Mail\PreorderRequest;
//use App\Models\PreorderRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Notifications\PreorderRequest;
use Illuminate\Support\Facades\Notification;

class PreorderRequestsController extends Controller
{
    public function store(Request $request){

        $data = $request->all();
        $validator = Validator::make($data,[
            'name' => 'required|string|min:1',
            'email' => 'required|email',
            'phone' => 'required|string',
            'message' => 'required|string|min:1',
        ]);

        if ($validator->fails())
            return response($this->formatResponse(false, 'validation_fail', $validator->errors()));

        Notification::route('mail', 'contactus.simplicity@gmail.com')->notify(new PreorderRequest($request));

//        Mail::send(new PreorderRequest($request));

        return $this->formatResponse(true, 'true', $data);
    }
}
