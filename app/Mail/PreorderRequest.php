<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PreorderRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($date)
    {
        $this->date = $date;
//        $this->name = $date["name"];
//        $this->email = $date["email"];
//        $this->phone = $date["phone"];
//        $this->message = $date["message"];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.preorder_mail')->from($this->date->email, $this->date->name)->to(env('ADMIN_EMAIL'))->subject('New preorder request!');
    }
}
