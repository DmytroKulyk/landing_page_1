<?php

namespace App\Traits;

trait FormatsResponse
{
    protected function formatResponse(bool $status, $message = null, $data = null): array
    {
        return [
            'status'  => $status ? 'success' : 'error',
            'data'    => $data,
            'message' => $message,
        ];
    }
}