<?php

namespace App\Traits;

trait UploadFile
{
    protected function uploadFileStorage($file,$path){

        //Create random name file
        $imageName = md5(uniqid());

        //Decode file
        $imageHash = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file));

        //Get file info
        $info_file = finfo_open();
        $mime_type = finfo_buffer($info_file, $imageHash, FILEINFO_MIME_TYPE);

        //Get file type
        $type      = explode( '/', $mime_type )[1];

        //Add create full file name
        $imageName = $imageName.'.'.$type;

        file_put_contents(base_path().'/storage/app/'.$path.$imageName, $imageHash);

        //Return file link
        return $path.$imageName;
    }

    protected function uploadFilePublic($file,$path){

        //Create random name file
        $imageName = md5(uniqid());

        //Decode file
        $imageHash = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file));

        //Get file info
        $info_file = finfo_open();
        $mime_type = finfo_buffer($info_file, $imageHash, FILEINFO_MIME_TYPE);

        //Get file type
        $type      = explode( '/', $mime_type )[1];

        //Add create full file name
        $imageName = $imageName.'.'.$type;

        file_put_contents(base_path().$path.$imageName, $imageHash);

        //Return file link
        return $path.$imageName;
    }
}
