$(document).ready(function () {
    $('#preorderform').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/api/preorder_request',
            data: $('#preorderform').serialize(),
            success: function (data) {
                $('#name').val('');
                $('#email').val('');
                $('#phone').val('');
                $('#message').val('');
            },
            error: function (request, status, error) {
                $('#name').val('');
                $('#mail').val('');
                $('#phone').val('');
                $('#message').val('');
            }
        });
    });
});